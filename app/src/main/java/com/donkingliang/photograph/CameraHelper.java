package com.donkingliang.photograph;

import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraHelper implements  SurfaceHolder.Callback{
    private static final String TAG = "CameraHelper";
    public   int width = 640;
    public   int height = 480;
    private int mCameraId;
    private Camera mCamera;


    private SurfaceView mSurfaceView;
    public CameraHelper(int cameraId, SurfaceView surfaceView) {
        mCameraId = cameraId;
        mSurfaceView=surfaceView;
        //保持屏幕长亮
        mSurfaceView.setKeepScreenOn(true);
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void switchCamera() {
        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        } else {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        }
        stopPreview();
        startPreview();
    }

    public int getCameraId() {
        return mCameraId;
    }

    public void stopPreview() {
        if (mCamera != null) {
            //预览数据回调接口
            mCamera.setPreviewCallback(null);
            //停止预览
            mCamera.stopPreview();
            //释放摄像头
            mCamera.release();
            mCamera = null;
        }
    }

    public void startPreview() {
        try {
            SurfaceHolder holder = mSurfaceView.getHolder();
            holder.addCallback(this);
            //获得camera对象
            mCamera = Camera.open(mCameraId);
            //配置camera的属性
            Camera.Parameters parameters = mCamera.getParameters();
            // 设置摄像头 图像传感器的角度、方向
            mCamera.setDisplayOrientation(90);
            //处理被拉伸问题
            Camera.Size preSize = getBestPreviewSize(width, height);
            if(preSize!=null){
                parameters.setPreviewSize(preSize.width, preSize.height);
            }else{
                parameters.setPreviewSize(width,height);
            }
            mCamera.setParameters(parameters);
            //设置预览画面
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }
    private Camera.Size getBestPreviewSize(int width, int height) {
        Camera.Size result = null;
        final Camera.Parameters p = mCamera.getParameters();
        //rate比例有可能是width/height或者height/width这里取大的比小的
        float rate = (float) Math.max(width, height)/ (float)Math.min(width, height);
        float tmp_diff;
        float min_diff = -1f;
        for (Camera.Size size : p.getSupportedPreviewSizes()) {
            float current_rate = (float) Math.max(size.width, size.height)/ (float)Math.min(size.width, size.height);
            tmp_diff = Math.abs(current_rate-rate);
            if( min_diff < 0){
                min_diff = tmp_diff ;
                result = size;
            }
            if( tmp_diff < min_diff ){
                min_diff = tmp_diff ;
                result = size;
            }
        }
        return result;
    }

}
